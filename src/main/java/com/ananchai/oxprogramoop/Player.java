/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.oxprogramoop;

/**
 *
 * @author Lenovo
 */
public class Player {

    String name;
    int Xwin;
    int Xlose;
    int Owin;
    int Olose;
    int draw;

    public Player(String name) {
        this.name = name;
    }

    public void xWin() {
        Xwin++;
    }

    public void xLose() {
        Xlose++;
    }

    public void oWin() {
        Owin++;
    }

    public void oLose() {
        Olose++;
    }

    public void oxDrew() {
        draw++;
    }
}
