/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.oxprogramoop;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Game {

    Scanner kb = new Scanner(System.in);

    Player playerX;
    Player playerO;
    Player turn;
    Table table;

    static String num;
    static String num2;
    static String num3;

    String Playagain = "";

    public Game() {
        playerX = new Player("X");
        playerO = new Player("O");
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showPlayagain() {
        System.out.println("");
        System.out.println("Do you want to play again ?");
        System.out.println("Enter: key 'y' for play again");
        System.out.println("Enter: any key for out program");
    }

    public void showTable() {
        table.showTable();
    }

    public void showTurn() {
        table.showTurn();
    }

    public void addXO() {
        table.addXO(num3);
    }

    public void checkWin() {
        table.checkWin();
    }

    public void input() {

        num = kb.next();
        num2 = kb.next();
        num3 = (num + num2);
    }

    public void newTable() {
        table.newTable();
    }

    public void newGame() {
        table = new Table(playerX, playerO);
        this.newTable();
    }

    public void run() {

        this.showWelcome();
        this.showTable();

        for (;;) {
            this.showTurn();
            this.input();
            this.addXO();

            this.checkWin();
            if (table.isFinish()) {
                showPlayagain();

                Playagain = kb.next();
                if (Playagain.equals("y")) {
                    this.newTable();
                    this.showTable();
                    newGame();
                }
                if (!Playagain.equals("y")) {
                    break;
                }

            }
        }
    }
}
