/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.oxprogramoop;

/**
 *
 * @author Lenovo
 */
public class Table {

    static int count = 1;

    static String one1 = "-";
    static String one2 = "-";
    static String one3 = "-";

    static String two1 = "-";
    static String two2 = "-";
    static String two3 = "-";

    static String thr1 = "-";
    static String thr2 = "-";
    static String thr3 = "-";

    static int check1 = 0;
    static int check2 = 0;
    static int check3 = 0;
    static int check4 = 0;
    static int check5 = 0;
    static int check6 = 0;
    static int check7 = 0;
    static int check8 = 0;
    static int check9 = 0;

    public boolean Finish = false;

    Player playerX;
    Player playerO;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
    }

    public void newTable() {
        count = 1;
        one1 = "-";
        one2 = "-";
        one3 = "-";

        two1 = "-";
        two2 = "-";
        two3 = "-";

        thr1 = "-";
        thr2 = "-";
        thr3 = "-";

        check1 = 0;
        check2 = 0;
        check3 = 0;
        check4 = 0;
        check5 = 0;
        check6 = 0;
        check7 = 0;
        check8 = 0;
        check9 = 0;

        Finish = false;
    }

    public void showTable() {
        System.out.println(" " + 1 + " " + 2 + " " + 3);
        System.out.println(1 + "" + one1 + " " + one2 + " " + one3);
        System.out.println(2 + "" + two1 + " " + two2 + " " + two3);
        System.out.println(3 + "" + thr1 + " " + thr2 + " " + thr3);
    }

    public void showTurn() {
        System.out.println("");

        if (count % 2 == 1) {
            System.out.println("X turn");
        }
        if (count % 2 == 0) {
            System.out.println("O turn");
        }

        System.out.println("Please input Row Col :");

    }

    public void addXO(String num3) {

        switch (num3) {

            case "11":
                if (check1 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    one1 = "X";
                }
                if (count % 2 == 0) {
                    one1 = "O";
                }
                showTable();
                check1++;
                count++;
                break;

            case "12":
                if (check2 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    one2 = "X";
                }
                if (count % 2 == 0) {
                    one2 = "O";
                }
                showTable();
                check2++;
                count++;
                break;

            case "13":
                if (check3 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    one3 = "X";
                }
                if (count % 2 == 0) {
                    one3 = "O";
                }
                showTable();
                check3++;
                count++;
                break;

            case "21":
                if (check4 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    two1 = "X";
                }
                if (count % 2 == 0) {
                    two1 = "O";
                }
                showTable();
                check4++;
                count++;
                break;

            case "22":
                if (check5 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    two2 = "X";
                }
                if (count % 2 == 0) {
                    two2 = "O";
                }
                showTable();
                check5++;
                count++;
                break;

            case "23":
                if (check6 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    two3 = "X";
                }
                if (count % 2 == 0) {
                    two3 = "O";
                }
                showTable();
                check6++;
                count++;
                break;

            case "31":
                if (check7 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    thr1 = "X";
                }
                if (count % 2 == 0) {
                    thr1 = "O";
                }
                showTable();
                check7++;
                count++;
                break;

            case "32":
                if (check8 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    thr2 = "X";
                }
                if (count % 2 == 0) {
                    thr2 = "O";
                }
                showTable();
                check8++;
                count++;
                break;

            case "33":
                if (check9 == 1) {
                    System.out.println("Error: table is not empty!");
                    break;
                }
                if (count % 2 == 1) {
                    thr3 = "X";
                }
                if (count % 2 == 0) {
                    thr3 = "O";
                }
                showTable();
                check9++;
                count++;
                break;

            default:
                System.out.println("Error: table is not empty!");

        }
    }

    public void ShowXwin() {
        System.out.println("Player X Win....");
        System.out.println("Bye bye ....");
    }

    public void ShowOwin() {
        System.out.println("Player O Win....");
        System.out.println("Bye bye ....");
    }

    public void checkWin() {

        if (one1.equals("X") && one2.equals("X") && one3.equals("X")) {
            ShowXwin();
            Finish = true;
        }
        if (two1.equals("X") && two2.equals("X") && two3.equals("X")) {
            ShowXwin();
            Finish = true;
        }
        if (thr1.equals("X") && thr2.equals("X") && thr3.equals("X")) {
            ShowXwin();
            Finish = true;
        }

        if (one1.equals("X") && two1.equals("X") && thr1.equals("X")) {
            ShowXwin();
            Finish = true;
        }
        if (one2.equals("X") && two2.equals("X") && thr2.equals("X")) {
            ShowXwin();
            Finish = true;
        }
        if (one3.equals("X") && two3.equals("X") && thr3.equals("X")) {
            ShowXwin();
            Finish = true;
        }

        if (one1.equals("X") && two2.equals("X") && thr3.equals("X")) {
            ShowXwin();
            Finish = true;
        }
        if (one3.equals("X") && two2.equals("X") && thr1.equals("X")) {
            ShowXwin();
            Finish = true;
        }
        //-------------------------------------------------------------
        if (one1.equals("O") && one2.equals("O") && one3.equals("O")) {
            ShowOwin();
            Finish = true;
        }
        if (two1.equals("O") && two2.equals("O") && two3.equals("O")) {
            ShowOwin();
            Finish = true;
        }
        if (thr1.equals("O") && thr2.equals("O") && thr3.equals("O")) {
            ShowOwin();
            Finish = true;
        }

        if (one1.equals("O") && two1.equals("O") && thr1.equals("O")) {
            ShowOwin();
            Finish = true;
        }
        if (one2.equals("O") && two2.equals("O") && thr2.equals("O")) {
            ShowOwin();
            Finish = true;
        }
        if (one3.equals("O") && two3.equals("O") && thr3.equals("O")) {
            ShowOwin();
            Finish = true;
        }

        if (one1.equals("O") && two2.equals("O") && thr3.equals("O")) {
            ShowOwin();
            Finish = true;
        }
        if (one3.equals("O") && two2.equals("O") && thr1.equals("O")) {
            ShowOwin();
            Finish = true;
        }
        if (count == 10) {
            showDraw();
            Finish = true;
        }
    }

    public void showDraw() {
        System.out.println("Draw ....");
        System.out.println("Bye bye ....");
    }

    public boolean isFinish() {
        return Finish;
    }
}
