/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.oxprogramoop;

/**
 *
 * @author Lenovo
 */
public class TestTable {
    public static void main(String[] args) {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);
        
        table.addXO("12");
        table.addXO("11");
        table.addXO("13");
        table.addXO("22");
        table.addXO("23");
        table.addXO("33");
        
        
        table.checkWin();
        
        System.out.println(table.isFinish());
       
    }
}
