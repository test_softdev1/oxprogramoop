/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.ananchai.oxprogramoop.Player;
import com.ananchai.oxprogramoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Lenovo
 */
public class TestTable {

    public TestTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testcol1ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("12");
        table.addXO("21");
        table.addXO("22");
        table.addXO("31");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testcol2ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("12");
        table.addXO("13");
        table.addXO("22");
        table.addXO("23");
        table.addXO("32");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testcol3ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("13");
        table.addXO("12");
        table.addXO("23");
        table.addXO("22");
        table.addXO("33");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testrow1ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("21");
        table.addXO("12");
        table.addXO("22");
        table.addXO("13");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testrow2ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("21");
        table.addXO("11");
        table.addXO("22");
        table.addXO("12");
        table.addXO("23");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testrow3ByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("31");
        table.addXO("11");
        table.addXO("32");
        table.addXO("12");
        table.addXO("33");

        table.checkWin();

        assertEquals(true, table.isFinish());
    }

    @Test
    public void testleftdiagonalByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("12");
        table.addXO("22");
        table.addXO("13");
        table.addXO("33");

        table.checkWin();

        assertEquals(true, table.isFinish());
    }

    @Test
    public void testrightdiagonalByX() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("13");
        table.addXO("12");
        table.addXO("22");
        table.addXO("11");
        table.addXO("31");

        table.checkWin();

        assertEquals(true, table.isFinish());
    }

    @Test
    public void testcol1ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("12");
        table.addXO("11");
        table.addXO("22");
        table.addXO("21");
        table.addXO("23");
        table.addXO("31");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testcol2ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("12");
        table.addXO("21");
        table.addXO("22");
        table.addXO("23");
        table.addXO("32");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testcol3ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("13");
        table.addXO("21");
        table.addXO("23");
        table.addXO("22");
        table.addXO("33");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testrow1ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("21");
        table.addXO("11");
        table.addXO("22");
        table.addXO("12");
        table.addXO("31");
        table.addXO("13");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testrow2ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("21");
        table.addXO("12");
        table.addXO("22");
        table.addXO("31");
        table.addXO("23");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }

    @Test
    public void testrow3ByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("21");
        table.addXO("31");
        table.addXO("22");
        table.addXO("32");
        table.addXO("11");
        table.addXO("33");

        table.checkWin();

        assertEquals(true, table.isFinish());

    }
    @Test
    public void testleftdiagonalByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("12");
        table.addXO("11");
        table.addXO("13");
        table.addXO("22");
        table.addXO("23");
        table.addXO("33");

        table.checkWin();

        assertEquals(true, table.isFinish());
    }
    @Test
    public void testrightdiagonalByO() {
        Player x = new Player("X");
        Player o = new Player("O");
        Table table = new Table(x, o);

        table.newTable();

        table.addXO("11");
        table.addXO("13");
        table.addXO("12");
        table.addXO("22");
        table.addXO("21");
        table.addXO("31");

        table.checkWin();

        assertEquals(true, table.isFinish());
    }
}
